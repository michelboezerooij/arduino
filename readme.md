Open the `1-screen/sketch_screen.ino` file in Arduino and upload it to the ESP.

In the setup change `TFT_BLACK` to `TFT_GREEN`. When you run the sketch, the background of the text still is black.
Use this to play with the text and background color:
`tft.setTextColor(TFT_RED, TFT_BLACK);`

Now, to use a landscape oriented screen instead of the portrait we have now, play with:
```
tft.setRotation(0);
tft.setRotation(1);
tft.setRotation(3);
tft.setRotation(4);
```

There are 8 pre-defined colors:
```
TFT_BLACK
TFT_BLUE
TFT_RED
TFT_GREEN
TFT_CYAN
TFT_MAGENTA
TFT_YELLOW
TFT_WHITE
```

But you can mix much more colors using `tft.color565(r, g, b)`, just replace r, g, b with any value from 0 - 255.
